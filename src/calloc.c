/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/20 10:25:07 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 18:51:00 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void						*calloc(size_t count, size_t size)
{
	void					*ret;
	static pthread_mutex_t	mutex;

	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	if (!(ret = malloc(size * count)))
		return (NULL);
	ft_bzero(ret, size * count);
	pthread_mutex_unlock(&mutex);
	return (ret);
}
