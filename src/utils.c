/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/15 19:54:21 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 18:52:21 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

size_t			find_good_size(size_t size)
{
	size_t		ret;
	int			pagesize;

	ret = 0;
	if (size % 16 == 0)
		return (size);
	pagesize = getpagesize();
	while (ret < size)
		ret += getpagesize();
	return (ret);
}

t_zone			*allocate_zone(t_zone *zone, size_t msize)
{
	t_zone		*new;
	t_zone		*tmp;
	size_t		good_size;

	good_size = find_good_size(msize * 100);
	if (!(new = mmap(NULL, find_good_size(sizeof(t_zone) + (sizeof(t_mem)
		* ft_get_var()->num_list) + msize * 100), PROT_READ | PROT_WRITE,
		MAP_ANON | MAP_PRIVATE, -1, 0)))
		return (NULL);
	ft_memset(new, 0, sizeof(t_zone));
	new->mem = (void*)new + (sizeof(t_zone));
	ft_memset(new->mem, 0, sizeof(t_mem) * ft_get_var()->num_list);
	new->addr = (void*)new + (sizeof(t_zone) +
	(sizeof(t_mem) * ft_get_var()->num_list));
	new->head_offset = new->addr;
	new->end_offset = new->addr + msize * 100;
	if (!zone)
		return (new);
	tmp = zone;
	while (tmp->nxt)
		tmp = tmp->nxt;
	tmp->nxt = new;
	return (zone);
}

t_large			*add_allocation_to_large_list(void *allocated,
				size_t size, t_large *list)
{
	t_large		*new;
	t_large		*tmp;

	if (!(new = mmap(NULL, sizeof(t_large), PROT_READ | PROT_WRITE,
		MAP_ANON | MAP_PRIVATE, -1, 0)))
		return (NULL);
	new->addr = allocated;
	new->size = size;
	new->nxt = NULL;
	if (!list)
		return (new);
	tmp = list;
	while (tmp->nxt)
		tmp = tmp->nxt;
	tmp->nxt = new;
	return (list);
}

t_large			*remove_from_list(t_large *list, t_large *mayon)
{
	t_large		*tmp;

	if (!mayon)
		return (list);
	if (!list)
		return (NULL);
	tmp = list;
	if (list == mayon)
	{
		tmp = mayon->nxt;
		if ((munmap(mayon, sizeof(t_large))) == -1)
			ft_putendl("Memory unmapping failed");
		return (tmp);
	}
	while (tmp->nxt != NULL && tmp->nxt != mayon)
		tmp = tmp->nxt;
	if (tmp->nxt == mayon)
	{
		tmp->nxt = mayon->nxt;
		if ((munmap(mayon, sizeof(t_large))) == -1)
			ft_putendl("Memory unmapping failed");
	}
	return (list);
}
