/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem_hex.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:05:01 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 19:30:37 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void						ft_putnbr_hex(long long n)
{
	char					base[16];

	ft_memcpy(base, "0123456789ABCDEF", 16);
	if (n >= 16)
		ft_putnbr_hex(n / 16);
	ft_putchar(base[n % 16]);
}

static int					print_memory(void *ptr, size_t size)
{
	size_t					i;
	int						a;
	unsigned char			*str;

	i = 0;
	str = (unsigned char*)ptr;
	while ((i * 16) < size)
	{
		ft_malloc_itoa_base((size_t)str + (i * 16));
		ft_putstr(" : ");
		a = -1;
		while (++a < 16)
		{
			if ((unsigned char)str[(i * 16) + a] < 16)
				ft_putnbr(0);
			ft_putnbr_hex((long)str[(i * 16) + a]);
			if (a < 15)
				ft_putchar(' ');
		}
		ft_putchar('\n');
		i++;
	}
	return (1);
}

static int					show_us_tiny_med(t_zone *zone, void *ptr)
{
	t_zone					*tmp;
	int						i;

	if (!zone || zone->num_mem <= 0)
		return (0);
	tmp = zone;
	while (tmp)
	{
		i = -1;
		while (++i < tmp->num_mem)
		{
			if (tmp->mem[i].addr == ptr)
				return (print_memory(ptr, tmp->mem[i].size));
		}
		tmp = tmp->nxt;
	}
	return (0);
}

int							show_us_large(t_large *list, void *ptr)
{
	t_large					*tmp;

	if (!list)
		return (0);
	tmp = list;
	while (tmp)
	{
		if (tmp->addr == ptr)
			return (print_memory(ptr, tmp->size));
		tmp = tmp->nxt;
	}
	return (0);
}

void						show_alloc_mem_hex(void *ptr)
{
	static pthread_mutex_t	mutex;
	int						i;

	i = 0;
	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	if (!(g_malloc_list = ft_get_var()))
		return ;
	i += show_us_tiny_med(g_malloc_list->zone_tiny, ptr);
	i += show_us_tiny_med(g_malloc_list->zone_med, ptr);
	i += show_us_large(g_malloc_list->large, ptr);
	if (i == 0)
		ft_putendl("Adress not found");
	pthread_mutex_unlock(&mutex);
}
