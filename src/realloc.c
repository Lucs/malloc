/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 20:01:22 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 18:59:51 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void						*extend(t_zone *tmp, size_t size, int i)
{
	if ((size_t)tmp->end_offset - ((size_t)tmp->mem[i].addr +
	size + 16) > size && (size_t)tmp->head_offset + size + 16 <
	(size_t)tmp->end_offset)
	{
		tmp->head_offset += size - tmp->mem[i].size + (16 -
		(((size_t)tmp->head_offset
		+ size - tmp->mem[i].size) % 16));
		tmp->mem[i].size = size;
		return (tmp->mem[i].addr);
	}
	return (NULL);
}

void						*try_to_extend(void *ptr,
							size_t size, t_zone *zone)
{
	t_zone					*tmp;
	void					*ret;
	int						i;

	tmp = zone;
	while (tmp)
	{
		i = -1;
		while (++i < tmp->num_mem)
			if (tmp->mem[i].addr == ptr)
			{
				if (i == tmp->num_mem - 1)
				{
					if ((ret = extend(tmp, size, i)) != NULL)
						return (ret);
				}
			}
		tmp = tmp->nxt;
	}
	return (NULL);
}

static void					*realloc_bis(size_t size,
							size_t last_size, void *ptr)
{
	void					*ret;

	if (!(ret = malloc(size)))
		return (NULL);
	if (last_size == (size_t)-1 || last_size > size)
		last_size = size;
	if (ptr)
		ft_memcpy(ret, ptr, last_size);
	free(ptr);
	return (ret);
}

void						*realloc(void *ptr, size_t size)
{
	void					*ret;
	size_t					last_size;
	static pthread_mutex_t	mutex;

	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	if (!ptr)
		return (malloc(size));
	if (ptr && size == 0)
	{
		free(ptr);
		return (malloc(MALLOC_N));
	}
	if (!(g_malloc_list = ft_get_var()))
		return (NULL);
	if ((last_size = ft_get_last_size(ptr, g_malloc_list)) == (size_t)-1)
		return (NULL);
	if ((ret = try_to_extend(ptr, size, g_malloc_list->zone_tiny)) != NULL
		|| (ret = try_to_extend(ptr, size, g_malloc_list->zone_med)) != NULL)
		return (ret);
	ret = realloc_bis(size, last_size, ptr);
	pthread_mutex_unlock(&mutex);
	return (ret);
}
