/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 19:35:11 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 19:36:54 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void						remap_memory(t_mem *mem, int size)
{
	t_mem					ret[ft_get_var()->num_list];
	int						i;
	int						a;

	ft_memset(&ret, 0, sizeof(t_mem) * ft_get_var()->num_list);
	i = -1;
	a = 0;
	while (++i <= size)
	{
		if (mem[i].addr != NULL)
		{
			ret[a].addr = mem[i].addr;
			ret[a].size = mem[i].size;
			a++;
		}
	}
	ft_memcpy(mem, ret, sizeof(t_mem) * ft_get_var()->num_list);
}

t_zone						*free_tiny_and_med(void *addr, t_zone *zone)
{
	t_zone					*tmp;
	int						i;

	tmp = zone;
	while (tmp)
	{
		i = -1;
		while (++i < tmp->num_mem)
		{
			if (tmp->mem[i].addr == addr)
			{
				if (i == tmp->num_mem - 1)
					tmp->head_offset -= tmp->mem[i].size +
					(16 - (((size_t)tmp->head_offset - tmp->mem[i].size) % 16));
				ft_memset(&tmp->mem[i], 0, sizeof(t_mem));
				remap_memory(tmp->mem, tmp->num_mem);
				tmp->num_mem--;
			}
		}
		tmp = tmp->nxt;
	}
	return (zone);
}

t_large						*free_large(void *addr, t_large *large)
{
	t_large					*tmp;

	tmp = large;
	while (tmp)
	{
		if (tmp->addr == addr && tmp->size > 0)
		{
			if ((munmap(tmp->addr, tmp->size)) == -1)
				ft_putendl("MunmapErreur");
			large = remove_from_list(large, tmp);
			break ;
		}
		tmp = tmp->nxt;
	}
	return (large);
}

void						free(void *addr)
{
	static pthread_mutex_t	mutex;

	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	g_malloc_list = ft_get_var();
	if (!addr || !g_malloc_list)
		return ;
	if (g_malloc_list->zone_tiny)
		g_malloc_list->zone_tiny =
		free_tiny_and_med(addr, g_malloc_list->zone_tiny);
	if (g_malloc_list->zone_med)
		g_malloc_list->zone_med =
		free_tiny_and_med(addr, g_malloc_list->zone_med);
	if (g_malloc_list->large)
		g_malloc_list->large = free_large(addr, g_malloc_list->large);
	g_malloc_list->zone_tiny = delete_zone_main(g_malloc_list->zone_tiny,
	MALLOC_N, g_malloc_list);
	g_malloc_list->zone_med = delete_zone_main(g_malloc_list->zone_med,
	MALLOC_M, g_malloc_list);
	pthread_mutex_unlock(&mutex);
}
