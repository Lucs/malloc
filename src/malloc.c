/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 23:30:39 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 18:50:37 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_malloc					*ft_get_var(void)
{
	return (g_malloc_list);
}

void						*malloc_n(size_t size)
{
	if (!g_malloc_list->zone_tiny ||
		ft_is_zone_full(g_malloc_list->zone_tiny, size, g_malloc_list) == 1)
		g_malloc_list->zone_tiny = allocate_zone(g_malloc_list->zone_tiny,
		MALLOC_N);
	return (find_a_place(g_malloc_list->zone_tiny, size, g_malloc_list));
}

void						*malloc_m(size_t size)
{
	if (!g_malloc_list->zone_med ||
		ft_is_zone_full(g_malloc_list->zone_med, size, g_malloc_list) == 1)
		g_malloc_list->zone_med = allocate_zone(g_malloc_list->zone_med,
		MALLOC_M);
	return (find_a_place(g_malloc_list->zone_med, size, g_malloc_list));
}

void						*malloc_l(size_t size)
{
	void					*ret;

	ret = mmap(NULL, find_good_size(size), PROT_READ | PROT_WRITE,
	MAP_ANON | MAP_PRIVATE, -1, 0);
	if (ret == (void*)-1)
		return (NULL);
	if (!(g_malloc_list->large = add_allocation_to_large_list(ret,
		find_good_size(size), g_malloc_list->large)))
		ft_putendl("Erreur");
	return (ret);
}

void						*malloc(size_t size)
{
	void					*ret;
	static pthread_mutex_t	mutex;

	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	if (size <= 0)
		size = MALLOC_N;
	if (!g_malloc_list)
	{
		if ((g_malloc_list = mmap(NULL, sizeof(t_malloc), PROT_READ |
			PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0)) == (void*)-1)
			return (NULL);
		ft_memset(g_malloc_list, 0, sizeof(t_malloc));
		g_malloc_list->num_list = (getpagesize() -
		sizeof(t_zone)) / sizeof(t_mem);
	}
	if (size <= MALLOC_N)
		ret = malloc_n(size);
	else if (size <= MALLOC_M)
		ret = malloc_m(size);
	else
		ret = malloc_l(size);
	pthread_mutex_unlock(&mutex);
	return (ret);
}
