/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 23:32:28 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 18:45:05 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static t_zone		*delete_zone(t_zone *zone, t_zone *mayon, size_t msize)
{
	t_zone			*tmp;

	tmp = zone;
	if (zone == mayon)
	{
		zone = zone->nxt;
		if ((munmap(mayon, find_good_size(sizeof(t_zone) + (sizeof(t_mem) *
			ft_get_var()->num_list) + msize * 100))) == -1)
			ft_putstr_fd("Memory Unmaping Error", 2);
		return (zone);
	}
	while (tmp->nxt != NULL && tmp->nxt != mayon)
		tmp = tmp->nxt;
	if (tmp->nxt != NULL)
	{
		tmp->nxt = mayon->nxt;
		if ((munmap(mayon, find_good_size(sizeof(t_zone) + (sizeof(t_mem) *
			ft_get_var()->num_list) + msize * 100))) == -1)
			ft_putstr_fd("Memory Unmaping Error", 2);
	}
	return (zone);
}

t_zone				*delete_zone_main(t_zone *zone, size_t msize, t_malloc *g)
{
	t_zone			*tmp;
	t_zone			*tmpmem;

	if (!zone)
		return (zone);
	tmp = zone;
	tmpmem = NULL;
	while (tmp)
	{
		if (tmp != zone)
		{
			if (tmp->num_mem <= 0 && tmpmem != NULL
			&& ft_is_zone_full(tmpmem, msize, g) == 0)
				return (delete_zone(zone, tmp, msize));
		}
		tmpmem = tmp;
		tmp = tmp->nxt;
	}
	return (zone);
}
