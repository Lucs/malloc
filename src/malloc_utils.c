/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 23:15:41 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/23 23:42:18 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

unsigned char		ft_is_zone_full(t_zone *zone, size_t size, t_malloc *g)
{
	t_zone			*tmp;

	tmp = zone;
	while (tmp)
	{
		if ((size_t)tmp->head_offset + size < (size_t)tmp->end_offset &&
				tmp->num_mem < g->num_list)
			return (0);
		tmp = tmp->nxt;
	}
	return (1);
}

static void			*insert_first_addr(t_zone *zone,
					int num, size_t insert_size)
{
	t_mem			tmp[ft_get_var()->num_list];
	int				a;

	tmp[0].addr = zone->addr + (16 - ((size_t)zone->addr % 16));
	tmp[0].size = insert_size;
	a = 0;
	while (++a <= num)
	{
		tmp[a].addr = zone->mem[a - 1].addr;
		tmp[a].size = zone->mem[a - 1].size;
	}
	ft_memcpy(zone->mem, tmp, sizeof(t_mem) * (num + 1));
	zone->num_mem++;
	return (zone->mem[0].addr);
}

static void			*insert_addr(t_zone *zone, int num,
					size_t insert_size, int i)
{
	t_mem			tmp[ft_get_var()->num_list];
	void			*ret;
	int				a;

	a = 0;
	while (a <= i)
	{
		tmp[a].addr = zone->mem[a].addr;
		tmp[a].size = zone->mem[a].size;
		a++;
	}
	tmp[a].addr = zone->mem[i].addr + zone->mem[i].size +
	(16 - (((size_t)zone->mem[i].addr + zone->mem[i].size) % 16));
	tmp[a].size = insert_size;
	ret = tmp[a].addr;
	a++;
	while (a <= num)
	{
		tmp[a].addr = zone->mem[a - 1].addr;
		tmp[a].size = zone->mem[a - 1].size;
		a++;
	}
	ft_memcpy(zone->mem, tmp, sizeof(t_mem) * (num + 1));
	zone->num_mem++;
	return (ret);
}

static void			*get_a_hole(t_zone *zone,
					int num, size_t insert_size, t_malloc *g)
{
	int				i;
	size_t			size;

	if (!zone)
		return (NULL);
	i = -1;
	if (zone->addr != zone->mem[0].addr &&
		zone->mem[0].addr != NULL && zone->num_mem < g->num_list)
		if ((size_t)zone->mem[0].addr - (size_t)zone->addr >= insert_size + 16)
			return (insert_first_addr(zone, num, insert_size));
	while (++i < num - 1 && zone->num_mem < g->num_list)
	{
		size = (size_t)zone->mem[i + 1].addr -
		((size_t)zone->mem[i].addr + zone->mem[i].size);
		if (insert_size + 16 <= size)
			return (insert_addr(zone, num, insert_size, i));
	}
	return (NULL);
}

void				*find_a_place(t_zone *zone, size_t size, t_malloc *g)
{
	t_zone			*tmp;
	void			*ret;

	tmp = zone;
	while (tmp)
	{
		ret = get_a_hole(tmp, tmp->num_mem, size, g);
		if (ret != NULL)
			return (ret);
		if ((size_t)tmp->head_offset + size + 16 <
			(size_t)tmp->end_offset && tmp->num_mem < g->num_list)
		{
			ret = tmp->head_offset + (16 - ((size_t)tmp->head_offset % 16));
			tmp->head_offset += size + (16 - ((size_t)tmp->head_offset % 16));
			tmp->mem[tmp->num_mem].addr = ret;
			tmp->mem[tmp->num_mem].size = size;
			tmp->num_mem++;
			return (ret);
		}
		tmp = tmp->nxt;
	}
	return (NULL);
}
