/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 16:13:34 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 19:06:39 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void						ft_malloc_itoa_base(size_t n)
{
	char					ret[64];
	char					base[16];
	int						i;
	int						imem;
	long					nmem;

	ft_memcpy(base, "0123456789ABCDEF", 16);
	nmem = n;
	i = 0;
	while (nmem != 0)
	{
		i++;
		nmem /= 16;
	}
	imem = i;
	ret[i] = 0;
	i--;
	while (i >= 0)
	{
		ret[i--] = base[(n % 16)];
		n /= 16;
	}
	ft_putstr("0x");
	ft_putstr(ret);
}

static void					show_us_tiny_med(t_zone *zone, char *str)
{
	t_zone					*tmp;
	int						i;

	if (!zone || zone->num_mem <= 0)
		return ;
	tmp = zone;
	while (tmp)
	{
		i = -1;
		ft_putstr(str);
		ft_malloc_itoa_base((size_t)tmp->addr);
		ft_putchar('\n');
		while (++i < tmp->num_mem)
		{
			ft_malloc_itoa_base((size_t)tmp->mem[i].addr);
			ft_putstr(" - ");
			ft_malloc_itoa_base((size_t)tmp->mem[i].addr + tmp->mem[i].size);
			ft_putstr(" : ");
			ft_putnbr(tmp->mem[i].size);
			ft_putendl(" octets");
		}
		tmp = tmp->nxt;
	}
}

static void					show_us_large(t_large *list)
{
	t_large					*tmp;

	if (!list)
		return ;
	tmp = list;
	while (tmp)
	{
		ft_putstr("LARGE : ");
		ft_malloc_itoa_base((size_t)tmp->addr);
		ft_putchar('\n');
		ft_malloc_itoa_base((size_t)tmp->addr);
		ft_putstr(" - ");
		ft_malloc_itoa_base((size_t)tmp->addr + tmp->size);
		ft_putstr(" : ");
		ft_putnbr(tmp->size);
		ft_putendl(" octets");
		tmp = tmp->nxt;
	}
}

void						show_alloc_mem(void)
{
	static pthread_mutex_t	mutex;

	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	if (!(g_malloc_list = ft_get_var()))
		return ;
	show_us_tiny_med(g_malloc_list->zone_tiny, "TINY : ");
	show_us_tiny_med(g_malloc_list->zone_med, "SMALL : ");
	show_us_large(g_malloc_list->large);
	pthread_mutex_unlock(&mutex);
}
