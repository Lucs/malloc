/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_last_size.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 23:50:51 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 17:37:28 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static size_t		ft_get_last_size_large(void *addr, t_large *list)
{
	t_large			*tmp;

	if (!addr || !list)
		return (-1);
	tmp = list;
	while (tmp)
	{
		if (tmp->addr == addr)
			return (tmp->size);
		tmp = tmp->nxt;
	}
	return (-1);
}

static size_t		ft_get_last_size_tiny_med(void *addr, t_zone *list)
{
	t_zone			*tmp;
	int				i;

	if (!addr || !list)
		return (-1);
	tmp = list;
	while (tmp)
	{
		i = -1;
		while (++i < tmp->num_mem)
			if (tmp->mem[i].addr == addr)
				return (tmp->mem[i].size);
		tmp = tmp->nxt;
	}
	return (-1);
}

size_t				ft_get_last_size(void *addr, t_malloc *list)
{
	size_t			ret;

	if ((ret = ft_get_last_size_tiny_med(addr,
		list->zone_tiny)) != (size_t)-1)
		return (ret);
	else if ((ret = ft_get_last_size_tiny_med(addr,
			list->zone_med)) != (size_t)-1)
		return (ret);
	else
		return (ft_get_last_size_large(addr, list->large));
	return (-1);
}
