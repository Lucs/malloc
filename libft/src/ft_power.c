/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 17:11:09 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 18:42:40 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned long		ft_upower(unsigned long n, int p)
{
	unsigned long	nmem;

	nmem = n;
	if (p == 1)
		return (n);
	else if (p == 0)
		return (1);
	while (p-- > 1)
		n = n * nmem;
	return (n);
}

int					ft_power(int n, int p)
{
	int				nmem;

	nmem = n;
	if (p == 1)
		return (n);
	else if (p == 0)
		return (1);
	while (p-- > 1)
		n = n * nmem;
	return (n);
}
