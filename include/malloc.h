/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 21:18:21 by lcharbon          #+#    #+#             */
/*   Updated: 2018/10/24 19:20:29 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H
# define MALLOC_N 1024
# define MALLOC_M 40960
# include <sys/mman.h>
# include <sys/types.h>
# include <unistd.h>
# include <pthread.h>
# include "libft.h"

typedef struct		s_mem
{
	void			*addr;
	size_t			size;
}					t_mem;

typedef struct		s_zone
{
	void			*head_offset;
	void			*end_offset;
	t_mem			*mem;
	int				num_mem;
	void			*addr;
	unsigned char	need_free;
	struct s_zone	*nxt;
}					t_zone;

typedef struct		s_large
{
	void			*addr;
	size_t			size;
	struct s_large	*nxt;
}					t_large;

typedef struct		s_malloc
{
	t_zone			*zone_tiny;
	t_zone			*zone_med;
	t_large			*large;
	int				num_list;
}					t_malloc;

static t_malloc		*g_malloc_list = NULL;

void				*calloc(size_t count, size_t size);
void				free(void *addr);
void				*malloc(size_t size);
void				*realloc(void *ptr, size_t size);
void				show_alloc_mem();
t_zone				*allocate_zone(t_zone *zone, size_t size);
size_t				find_good_size(size_t size);
t_large				*add_allocation_to_large_list(void *allocated,
					size_t size, t_large *list);
t_large				*remove_from_list(t_large *list, t_large *mayon);
t_malloc			*ft_get_var();
void				ft_malloc_itoa_base(size_t n);
void				memory_dump();
unsigned char		ft_is_zone_full(t_zone *zone, size_t size, t_malloc *g);
void				*malloc_l(size_t size);
void				ft_set_null();
void				*find_a_place(t_zone *zone, size_t size, t_malloc *g);
t_zone				*delete_zone_main(t_zone *zone, size_t msize, t_malloc *g);
size_t				ft_get_last_size(void *addr, t_malloc *list);
void				show_alloc_mem_hex(void *ptr);

#endif
