# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lcharbon <lcharbon@student42.fr>           +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 11:21:25 by lcharbon          #+#    #+#              #
#    Updated: 2018/10/24 19:35:12 by lcharbon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = libft_malloc_$(HOSTTYPE).so
END_NAME=libft_malloc.so
FLAG = -Wall -Werror -Wextra -g
CC = gcc

SRC_P = ./src/
OBJ_P = ./obj/
INC_P = ./include/ ./libft/include/
LIB_P	= ./libft/

LIB_N	= -lft
SRC_N = malloc.c utils.c show_alloc_mem.c free.c realloc.c calloc.c \
		malloc_utils.c free_utils.c get_last_size.c show_alloc_mem_hex.c

OBJ_N =$(SRC_N:.c=.o)
SRC = $(addprefix $(SRC_P),$(SRC_N))
OBJ = $(addprefix $(OBJ_P),$(OBJ_N))
INC = $(addprefix -I,$(INC_P))
LIB		= $(addprefix -L,$(LIB_P))

all : lib
	@make $(NAME)

$(NAME): $(OBJ)
	@$(CC) $(LIB) $(LIB_N) -shared -fPIC -o $(NAME) $^
	@ln -s $(NAME) $(END_NAME)

lib:
	@echo "\033[1mMake libft\033[0;0m"
	@make -C $(LIB_P)

$(OBJ_P)%.o: $(SRC_P)%.c $(INCLUDE)
	@mkdir -p $(OBJ_P)
	@$(CC) $(FLAG) -c $^ $(INC) -o $@
	@printf "\033[0;33m▓\033[0;0m"

clean:
	@rm -f $(OBJ)
	@rm -rf $(END_NAME)
	@rmdir $(OBJ_P) 2> /dev/null || echo "" > /dev/null
	@make -C ./libft/ clean
	@echo "\033[1m\x1b[33mRemove......... |\x1b[32m| done\x1b[37m"

uclean:
	@rm -f $(OBJ)
	@rm -f $(END_NAME)
	@rmdir $(OBJ_P) 2> /dev/null || echo "" > /dev/null
	@echo "\033[1m\x1b[33mRemove......... |\x1b[32m| done\x1b[37m"

fclean: clean
	@rm -f $(NAME) $(END_NAME)
	@make -C ./libft/ fclean
	@echo "\033[1m\x1b[33mRemove all..... |\x1b[32m| done\x1b[37m\033[0;0m"

re: fclean all

ure : uclean all
